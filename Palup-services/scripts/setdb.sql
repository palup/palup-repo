USE paldb;

CREATE TABLE IF NOT EXISTS events (
  ID          DOUBLE NOT NULL AUTO_INCREMENT,
  TITLE       CHAR(100),
  TIME        LONG,
  LATITUDE    DOUBLE,
  LONGITUDE   DOUBLE,
  DESCRIPTION TEXT(1000),
  LABEL       TEXT(32),
  PRIMARY KEY (ID)
);

CREATE TABLE IF NOT EXISTS labels (
  id   CHAR(100),
  name CHAR(100),
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS labelrelations (
  parent CHAR(100),
  child  CHAR(100),
  PRIMARY KEY (parent, child)
);

CREATE TABLE IF NOT EXISTS users (
  name      CHAR(100),
  email     CHAR(100),
  photo_url TEXT,
  PRIMARY KEY (email)
);

CREATE TABLE IF NOT EXISTS interest (
  email CHAR(100),
  label CHAR(100),
  PRIMARY KEY (email, label)
)