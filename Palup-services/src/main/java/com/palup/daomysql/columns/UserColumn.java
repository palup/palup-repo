package com.palup.daomysql.columns;

/**
 * Columns in user table
 * <p>
 * Created by kuldeep on 15/12/15.
 */
public enum UserColumn {

    NAME, EMAIL, PHOTO_URL;

    public int columnNumber() {
        return ordinal() + 1;
    }
}
