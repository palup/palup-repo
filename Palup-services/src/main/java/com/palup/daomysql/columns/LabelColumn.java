package com.palup.daomysql.columns;

/**
 * Columns for labels table
 * <p>
 * Created by Kuldeep Yadav on 08-Dec-15.
 */
public enum LabelColumn {
    // Order of the enum elements is to be kept
    // consistent with columns of event table
    ID,
    NAME
}
