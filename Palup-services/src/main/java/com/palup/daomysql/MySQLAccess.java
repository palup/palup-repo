package com.palup.daomysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * APIs to setup a connection with mysql database.
 * <p>
 * Created by  Kuldeep Yadav on 21-Oct-15.
 */
public abstract class MySQLAccess {

    /**
     * Connection to MySQL server.
     */
    protected Connection connection;
    /**
     * User name to access database.
     */
    private String user = "kuldeep";
    /**
     * Password for the user
     */
    private String password = "kd5534kd";
    /**
     * Database to access
     */
    private String url = "jdbc:mysql://localhost/paldb";

    public MySQLAccess(String user, String password, String url) throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        this.connection = DriverManager.getConnection(url, user, password);
    }

    public MySQLAccess() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        this.connection = DriverManager.getConnection(url, user, password);
    }

    /**
     * @return connection to mysql database
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * Close connection to dbms.
     *
     * @throws SQLException
     */
    public void terminateConnection() throws SQLException {
        connection.close();
    }
}
