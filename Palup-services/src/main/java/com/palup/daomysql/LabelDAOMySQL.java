package com.palup.daomysql;

import com.palup.daomysql.columns.LabelColumn;
import com.palup.daomysql.columns.LabelRelationColumn;
import com.palup.entity.Label;
import com.palup.entity.LabelCreationDetails;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * MySQL implementation for LabelDAO
 * <p>
 * Created by Kuldeep Yadav on 08-Dec-15.
 */
public class LabelDAOMySQL extends MySQLAccess implements com.palup.dao.LabelDAO {

    public LabelDAOMySQL(String user, String password, String url) throws ClassNotFoundException, SQLException {
        super(user, password, url);
    }

    public LabelDAOMySQL() throws SQLException, ClassNotFoundException {
        super();
    }

    @Override
    public Label createLabel(LabelCreationDetails label) throws SQLException {

        PreparedStatement statement = connection.prepareStatement("INSERT INTO paldb.labels VALUES (?, ?)");
        String labelName = label.getName();
        String labelId = makeLabelId(label.getName());
        statement.setString(LabelColumn.ID.ordinal() + 1, labelId);
        statement.setString(LabelColumn.NAME.ordinal() + 1, labelName);
        statement.execute();
        return new Label(labelName, labelId);
    }

    @Override
    public void setLabelRelation(LabelCreationDetails label) throws SQLException {

        PreparedStatement statement = connection.prepareStatement("INSERT INTO paldb.labelrelations VALUES (?, ?)");
        statement.setString(LabelRelationColumn.PARENT.ordinal() + 1, label.getParentLabelId());
        statement.setString(LabelRelationColumn.CHILD.ordinal() + 1, makeLabelId(label.getName()));
        statement.execute();
    }

    /**
     * @param labelName, name of label
     * @return id of label with given name
     */
    private String makeLabelId(String labelName) {

        // TODO: handle cases when name have multiple space between words
        return labelName.toLowerCase();
    }

    @Override
    public Label readLabel(String id) throws SQLException {

        PreparedStatement statement = connection.prepareStatement("SELECT * FROM paldb.labels WHERE id = ?");
        statement.setString(1, id);
        ResultSet result = statement.executeQuery();
        result.next();
        String name = result.getString(LabelColumn.NAME.toString());
        return new Label(name, id);
    }

    @Override
    public List<Label> readLabels() throws SQLException {

        PreparedStatement statement = connection.prepareStatement("SELECT * FROM paldb.labels");
        ResultSet result = statement.executeQuery();
        List<Label> labels = new ArrayList<>();
        while (result.next()) {
            String id = result.getString(LabelColumn.ID.toString());
            String name = result.getString(LabelColumn.NAME.toString());
            labels.add(new Label(name, id));
        }

        return labels;
    }

}
