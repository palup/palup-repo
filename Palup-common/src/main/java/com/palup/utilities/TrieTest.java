package com.palup.utilities;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Junit test for Trie
 * <p>
 * Created by kuldeep on 12/12/15.
 */
public class TrieTest {

    @Test
    public void testCheckTyped() throws Exception {

        char[] alphabet = new char[]{'b', 'i', 'o', 'x', 'l'};
        Trie tire = new Trie(alphabet);
        tire.insert("box");
        tire.insert("ox");
        tire.insert("bool");
        assertTrue(tire.isPresent("box"));
        assertFalse(tire.isPresent("lol"));
        tire.delete("box");
        assertFalse(tire.isPresent("box"));
        assertTrue(tire.isPresent("ox"));

        // if (tire.checkTyped('b'))

        assertFalse(tire.checkTyped('b'));
        assertFalse(tire.checkTyped('o'));
        assertFalse(tire.checkTyped('o'));
        assertFalse(tire.checkTyped('x'));
        assertFalse(tire.checkDeleted());
        assertTrue(tire.checkTyped('l'));
        assertFalse(tire.checkTyped('k'));
        assertTrue(tire.checkDeleted());
        assertFalse(tire.checkDeleted());
        assertFalse(tire.checkDeleted());
        assertFalse(tire.checkDeleted());
        assertFalse(tire.checkDeleted());
    }
}