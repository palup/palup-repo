package com.palup.utilities;

import com.palup.entity.Location;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Junit test for MapUtils
 * <p>
 * Created by kuldeep on 02/01/16.
 */
public class MapUtilsTest {

    @Test
    public void testDisplacement() throws Exception {


        int longDistanceErrorLimit = 1000; // 1000 meters error can be tolerated for 6 digit distance and more
        int mediumDistanceErrorLimit = 500; // 500 meters error can be tolerated for 4 to 5 digit distance
        int smallDistanceErrorLimit = 100; // 100 meters error can be tolerated for up to 3 digit distance


        Location baptistHospital = new Location(13.035311, 77.589934);
        Location anugraha = new Location(13.034335, 77.589155);
        double displacement = MapUtils.displacement(baptistHospital, anugraha);
        // Anugraha to Baptist Hospital
        int googleMapDisplacement = 146;
        int error = (int) Math.abs(displacement - googleMapDisplacement);
        assertTrue(error < smallDistanceErrorLimit);

        Location kempagowdaInternationAirport = new Location(13.204826, 77.707744);
        displacement = MapUtils.displacement(anugraha, kempagowdaInternationAirport);
        // Anugraha to KIA
        googleMapDisplacement = 22870;
        error = (int) Math.abs(displacement - googleMapDisplacement);
        assertTrue(error < mediumDistanceErrorLimit);

        Location igiDelhi = new Location(28.556455, 77.099969);
        displacement = MapUtils.displacement(kempagowdaInternationAirport, igiDelhi); // 1708 km
        // IGI Delhi airport to KIA Bangalore
        googleMapDisplacement = 1708190;
        error = (int) Math.abs(displacement - googleMapDisplacement);
        assertTrue(error < longDistanceErrorLimit);

        Location microsoftLavelleOffice = new Location(12.966334, 77.596315);
        displacement = MapUtils.displacement(anugraha, microsoftLavelleOffice);
        // anugraha to microsoft
        googleMapDisplacement = 7590;
        error = (int) Math.abs(displacement - googleMapDisplacement);
        assertTrue(error < mediumDistanceErrorLimit);
    }

    @Test
    public void testGetLatitudeBound() {

        Location anugraha = new Location(13.034335, 77.589155);
        double errorLimit = 0.01;

        MapUtils.Pair<Double> bound = MapUtils.getLatitudeBounds(anugraha, 10 * 1000); // 10 KMs
        assertTrue(isApproximate(bound.getFirst(), 12.944797, errorLimit));
        assertTrue(isApproximate(bound.getSecond(), 13.124432, errorLimit));

        bound = MapUtils.getLatitudeBounds(anugraha, 1000 * 1000);  // 1000 KMs
        assertTrue(isApproximate(bound.getFirst(), 4.042076, errorLimit));
        assertTrue(isApproximate(bound.getSecond(), 22.035352, errorLimit));
    }

    @Test
    public void testGetLongitudeBound() {

        Location anugraha = new Location(13.034335, 77.589155);
        double errorLimit = 0.01;

        MapUtils.Pair<Double> bound = MapUtils.getLongitudeBounds(anugraha, 10 * 1000); // 10 KMs
        assertTrue(isApproximate(bound.getFirst(), 77.496816, errorLimit));
        assertTrue(isApproximate(bound.getSecond(), 77.681488, errorLimit));

        bound = MapUtils.getLongitudeBounds(anugraha, 100 * 1000);  // 100 KMs
        assertTrue(isApproximate(bound.getFirst(), 76.666040, 0.1));
        assertTrue(isApproximate(bound.getSecond(), 78.512698, 0.1));
    }

    /**
     * @param trueValue     value we got
     * @param expectedValue expected value
     * @param errorLimit    error allowed
     * @return true if trueValue is close enough to expected value.
     */
    private boolean isApproximate(double trueValue, double expectedValue, double errorLimit) {
        return Math.abs(expectedValue - trueValue) < errorLimit;
    }
}