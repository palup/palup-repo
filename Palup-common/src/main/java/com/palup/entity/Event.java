package com.palup.entity;

/**
 * Full details of an event.
 * <p>
 * Created by Kuldeep Yadav on 20-Oct-15.
 */
public class Event extends EventBrief {

    /**
     * Detailed description.
     */
    private String description;

    /**
     * For Jackson support.
     */
    public Event() {
    }

    public Event(long id, String title, long time, Location location, String description, String label) {
        super(id, title, time, location, label);
        this.description = description;
    }

    /**
     * @return detailed description of event.
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description of event.
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
