package com.palup.entity;

/**
 * Status of entity in database
 * <p>
 * Created by kuldeep on 16/12/15.
 */
public enum EntityStatus {

    EXISTS, // entity exists in database
    NEW,    // new entity created

}
