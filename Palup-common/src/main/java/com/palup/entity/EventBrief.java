package com.palup.entity;

/**
 * Event in short. Fewer details are needed to show list
 * of events.
 * <p>
 * Created by Kuldeep Yadav on 20-Oct-15.
 */
public class EventBrief {

    /**
     * Id used to identify an event uniquely.
     */
    private long id;

    /**
     * Title in few words.
     */
    private String title;

    /**
     * Time when event is to be happen.
     */
    private long time;

    /**
     * Location where event is to happen
     */
    private Location location;

    /**
     * Label under which event have been placed
     */
    private String label;

    /**
     * For Jackson support.
     */
    public EventBrief() {
    }

    public EventBrief(long id, String title, long time, Location location, String label) {
        this.id = id;
        this.title = title;
        this.time = time;
        this.location = location;
        this.label = label;
    }

    /**
     * @return Id of event
     */
    public long getId() {
        return id;
    }

    /**
     * @return title of event.
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title of event
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return time when event will take place
     */
    public long getTime() {
        return time;
    }

    /**
     * @param time when event will take place
     */
    public void setTime(long time) {
        this.time = time;
    }

    /**
     * @return location coordinates where event is to be held.
     */
    public Location getLocation() {
        return location;
    }

    /**
     * @param location coordinates where event is to be held.
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return ("{ id : " + id + ", title : " + title + ", location : " + location + ", time : " + time);
    }

    /**
     * @return label event have been tagged with
     */
    public String getLabel() {
        return label;
    }

    /**
     * @param label to tag the event
     */
    public void setLabel(String label) {
        this.label = label;
    }
}
