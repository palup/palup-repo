package com.palup.entity;

/**
 * Location, coordinates of a point on map
 * <p>
 * Created by  Kuldeep Yadav on 22-Oct-15.
 */
public class Location {

    /**
     * Latitude of the location
     */
    private double latitude;

    /**
     * Longitude of the location
     */
    private double longitude;

    /**
     * For Jackson support.
     */
    public Location() {
    }

    public Location(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    /**
     * @return latitude of the location
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * @return longitude of location
     */
    public double getLongitude() {
        return longitude;
    }

    @Override
    public String toString() {
        return "{latitude : " + latitude + ", longitude : " + longitude + "}";
    }
}