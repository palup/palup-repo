package com.palup.entity;

/**
 * Details of event to be created.
 * <p>
 * Created by  Kuldeep Yadav on 21-Oct-15.
 */
public class EventCreationDetails {

    /**
     * Title of event in few words.
     */
    private String title;

    /**
     * Detailed description of event.
     */
    private String description;

    /**
     * Venue for event
     */
    private Location location;

    /**
     * Time of the event
     */
    private long time;

    /**
     * Id of label to be put on Event
     */
    private String label;

    /**
     * For Jackson support.
     */
    public EventCreationDetails() {
    }

    public EventCreationDetails(String title, String description, Location location, long time, String labelId) {
        this.title = title;
        this.description = description;
        this.location = location;
        this.time = time;
        this.label = labelId;
    }

    /**
     * @return title of event to be created.
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title of event to be created.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return description of event to be created.
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description of event to be created.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return location of event to be created.
     */
    public Location getLocation() {
        return location;
    }

    /**
     * @param location of event to be created.
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * @return time of the event.
     */
    public long getTime() {
        return time;
    }

    /**
     * Set event start time.
     *
     * @param time of the event.
     */
    public void setTime(long time) {
        this.time = time;
    }

    /**
     * @return id of label to be put on the event
     */
    public String getLabel() {
        return label;
    }

    /**
     * @param labelId id of label for event
     */
    public void setLabel(String labelId) {
        this.label = labelId;
    }
}
