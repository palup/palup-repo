package com.palup.crud;

import com.palup.dao.EventDAO;
import com.palup.daomysql.EventDAOMySQL;
import com.palup.entity.Event;
import com.palup.entity.EventBrief;
import com.palup.entity.EventCreationDetails;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

/**
 * CRUD support for events
 * <p/>
 * Created by Kuldeep Yadav on 14-Nov-15.
 */
@Path("/event")
public class EventCRUD {

    /**
     * Log debug info
     */
    private Logger logger = Logger.getLogger("Event CRUD");

    /**
     * Dummy function to test WEB-Services
     *
     * @return Http OK with a message
     */
    @GET
    public Response greet() {
        return Response.ok("Welcome to Pal-up Event Web APIs !!").build();
    }

    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(final EventCreationDetails event) throws SQLException, ClassNotFoundException {

        logger.info("Creating event ... ");
        EventDAO eventDAO = new EventDAOMySQL();
        eventDAO.createEvent(event);
        return Response.ok("Requested event created").build();
    }

    @GET
    @Path("/read/allevents")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EventBrief> allEvents() throws SQLException, ClassNotFoundException {

        EventDAO eventDAO = new EventDAOMySQL();
        return eventDAO.readBriefEvents();
    }

    @GET
    @Path("/read/event/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Event readEvent(@PathParam("id") final long id) throws SQLException, ClassNotFoundException {

        EventDAO eventDAO = new EventDAOMySQL();
        return eventDAO.readEvent(id);
    }
}
