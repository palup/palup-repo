package com.palup.crud;

import com.palup.dao.EventDAO;
import com.palup.dao.UserDAO;
import com.palup.daomysql.EventDAOMySQL;
import com.palup.daomysql.UserDAOMySQL;
import com.palup.entity.EventBrief;
import com.palup.entity.Interest;
import com.palup.entity.Location;
import com.palup.entity.UserCreationDetails;
import com.palup.utilities.MapUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

/**
 * CRUD support for User entities.
 * <p>
 * Created by kuldeep on 16/12/15.
 */
@Path("/user")
public class UserCRUD {

    /**
     * Log debug info
     */
    private Logger logger = Logger.getLogger("User CRUD");

    /**
     * Test methods for connection checks
     */
    @GET
    public Response greet() {
        return Response.ok("Welcome to Pal-up User Web APIs").build();
    }

    /**
     * @param details of user to be created
     */
    @Path("/create")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(UserCreationDetails details) throws SQLException, ClassNotFoundException {

        UserDAO userDAO = new UserDAOMySQL();
        userDAO.create(details);
        return Response.ok("User got created successfully").build();
    }

    @Path("/update")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(UserCreationDetails details) throws SQLException, ClassNotFoundException {

        UserDAO userDAO = new UserDAOMySQL();
        if (userDAO.isExistingUser(details.getEmail())) {
            /* if user already exists */
            userDAO.update(details);
            return Response.ok("existing user").status(Response.Status.NO_CONTENT).build();
        } else {
            /* create new user */
            userDAO.create(details);
            return Response.ok("new user").status(Response.Status.CREATED).build();
        }
    }

    @Path("/interest/get/{email}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Interest> interests(@PathParam("email") final String email) throws SQLException, ClassNotFoundException {

        InterestUtilities interestUtilities = new InterestUtilities();
        return interestUtilities.userInterests(email);
    }

    @Path("/interest/set/{email}")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response setInterests(@PathParam("email") final String email, Set<String> labels) throws SQLException, ClassNotFoundException {

        InterestUtilities interestUtilities = new InterestUtilities();
        interestUtilities.updateInterests(email, labels);
        return Response.ok("Interests modified successfully").build();
    }

    @Path("/event/suggest/{email}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public List<EventBrief> eventSuggestions(@PathParam("email") final String email, Location userLocation) throws SQLException, ClassNotFoundException {

        InterestUtilities interestUtilities = new InterestUtilities();
        List<Interest> interests = interestUtilities.userInterests(email);
        List<String> labels = readLabelsOfInterests(interests);

        // TODO : it is to be picked from user preferences
        int radiusBound = 10 * 1000; // 10 KMs

        MapUtils.Pair<Double> latitudeBounds = MapUtils.getLatitudeBounds(userLocation, radiusBound);
        MapUtils.Pair<Double> longitudeBounds = MapUtils.getLongitudeBounds(userLocation, radiusBound);

        EventDAO eventDAO = new EventDAOMySQL();
        return eventDAO.readRelevantEvents(labels, latitudeBounds, longitudeBounds);
    }

    /**
     * @param interests list
     * @return labels present in interests
     */
    private List<String> readLabelsOfInterests(List<Interest> interests) {

        List<String> labels = new ArrayList<String>();
        for (Interest interest : interests) {
            if (interest.isInterested())
                labels.add(interest.getLabel().getId());
        }
        return labels;
    }
}
