package in.co.palup.android;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.palup.entity.EventBrief;

import java.io.IOException;
import java.util.List;

import in.co.palup.android.adapter.EventArrayAdapter;
import in.co.palup.android.singleton.PalupSingleton;
import in.co.palup.android.utilities.Configuration;
import in.co.palup.android.volley.GsonRequest;


/**
 * Home activity
 */
public class PalupHome extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    /**
     * Request code for location permission
     */
    private static final int PERMISSION_REQUEST_CODE_LOCATION = 1;

    /**
     * Minimum displacement for event reload
     */
    private static final int MINIMUM_DISPLACEMENT_FOR_EVENT_RELOAD = 1000;  // 1000 meters

    /**
     * Tag for logger.
     */
    private final String TAG = "Home";
    /**
     * Large update interval for location provider
     */
    private final int LOCATION_UPDATE_INTERVAL = 60 * 1000;   // 60 seconds
    /**
     * Dialog to show when loading is going in background
     */
    private ProgressDialog progressDialog;
    /**
     * Google API client.
     */
    private GoogleApiClient googleApiClient;
    /**
     * Location request.
     */
    private LocationRequest locationRequest;
    /**
     * The location manager
     */
    private LocationManager locationManager;
    /**
     * Last known location of user/device
     */
    private Location lastLocation;

    /**
     * Configuration resource handle
     */
    private Configuration configuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_palup_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.configuration = new Configuration(getApplicationContext());

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Create new event");
                Intent intent = new Intent(getApplicationContext(), EventCreation.class);
                startActivity(intent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(SessionData.class.getName(), 0);
        String email = sharedPreferences.getString(SessionData.EMAIL.name(), null);
        Toast.makeText(getApplicationContext(), "Logged in with " + email, Toast.LENGTH_SHORT).show();

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        locationManager =
                (android.location.LocationManager) this.getSystemService(LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER)) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            this.startActivity(intent);
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_CODE_LOCATION);
        }

        progressDialog = createProgressDialog();
        locationRequest = createLocationRequest();
        lastLocation = new Location(LocationManager.GPS_PROVIDER);
    }

    /**
     * @return progress dialog instance
     */
    private ProgressDialog createProgressDialog() {
        ProgressDialog progressDialog = new ProgressDialog(PalupHome.this);
        progressDialog.setCancelable(true);
        progressDialog.setMessage("Loading event list ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(50);
        return progressDialog;
    }

    /**
     * Create location request.
     *
     * @return location request instance.
     */
    protected LocationRequest createLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(LOCATION_UPDATE_INTERVAL);
        locationRequest.setFastestInterval(LOCATION_UPDATE_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return locationRequest;
    }

    /**
     * Start location updates
     */
    private void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                googleApiClient, locationRequest, this);
    }

    /**
     * Stop location updates
     */
    private void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                googleApiClient, this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.palup_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_account) {
            Log.i(TAG, "open account");
        } else if (id == R.id.nav_interest) {
            Intent interestIntent = new Intent(this, InterestSelection.class);
            startActivity(interestIntent);
        } else if (id == R.id.nav_contact_us) {
            Log.i(TAG, "open contact us");
        } else if (id == R.id.nav_about) {
            Log.i(TAG, "open about us");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Get the events from server.
     * and populate them in home page as relevant suggestions.
     */
    private void suggestEvents(Location location) throws IOException {

        progressDialog.show();
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(SessionData.class.getName(), 0);
        String email = sharedPreferences.getString(SessionData.EMAIL.name(), null);
        Gson gson = new Gson();

        com.palup.entity.Location nativeLocationObject =
                new com.palup.entity.Location(location.getLatitude(), location.getLongitude());

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, error.toString());
                progressDialog.hide();
            }
        };

        Response.Listener<List<EventBrief>> listener = new Response.Listener<List<EventBrief>>() {
            @Override
            public void onResponse(List<EventBrief> response) {
                Log.i(TAG, "event list size :" + response.size());
                populateEventList(response);
                progressDialog.hide();
            }
        };

        String url = configuration.getScheme() + "://" + configuration.getAuthority() + "/user/event/suggest/" + email;
        GsonRequest<List<EventBrief>> eventListRequest = new GsonRequest<>(url, gson.toJson(nativeLocationObject),
                new TypeToken<List<EventBrief>>() {
                }.getType(), listener, errorListener);

        PalupSingleton.getInstance(getApplicationContext()).getRequestQueue().add(eventListRequest);
    }

    /**
     * Populate event list.
     *
     * @param events list to be shown in home page
     */
    private void populateEventList(List<EventBrief> events) {
        final EventArrayAdapter adapter = new EventArrayAdapter(getApplicationContext(), PalupHome.this,
                R.layout.event_list_item, events);
        ListView eventList = (ListView) findViewById(R.id.homepage_list);
        eventList.setAdapter(adapter);

        eventList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), EventDetails.class);
                EventBrief event = adapter.getItem(position);
                intent.putExtra("id", event.getId());
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        googleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(Bundle bundle) {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED && locationManager.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER)) {
            createLocationRequest();
            startLocationUpdates();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i("onLocationChanged", location.toString());
        lastLocation = new Location(LocationManager.PASSIVE_PROVIDER);
        Log.i("last location", lastLocation.toString());

        if (lastLocation.distanceTo(location) > MINIMUM_DISPLACEMENT_FOR_EVENT_RELOAD) {

            try {
                lastLocation = location;
                suggestEvents(location);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        stopLocationUpdates();
    }

}
