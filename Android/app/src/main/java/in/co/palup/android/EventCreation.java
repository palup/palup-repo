package in.co.palup.android;

import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.palup.entity.EventCreationDetails;
import com.palup.entity.Label;
import com.palup.entity.Location;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import in.co.palup.android.dialog.DatePickerFragment;
import in.co.palup.android.dialog.LabelCreationDialog;
import in.co.palup.android.dialog.TimePickerFragment;
import in.co.palup.android.location.MapUtils;
import in.co.palup.android.singleton.PalupSingleton;
import in.co.palup.android.utilities.Configuration;
import in.co.palup.android.volley.GsonRequest;
import in.co.palup.android.volley.SubmitRequest;

/**
 * Event creation activity.
 *
 * @author Kuldeep Yadav
 */
public class EventCreation extends FragmentActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        TimePickerFragment.OnCompleteListener,
        DatePickerFragment.OnCompleteListener,
        LabelCreationDialog.OnCompleteListener {

    /**
     * Tag for logger.
     */
    private static final String LOG_TAG = "Event Creation";
    // TODO : to be changed with some relevant bounds
    private static final LatLngBounds BOUNDS_INDIA = new LatLngBounds(
            new LatLng(6.0, 68.0),
            new LatLng(35.0, 98.0));

    /**
     * Request code to identify place picker response.
     */
    private final int PLACE_PICKER_REQUEST = 1;
    /**
     * GoogleApiClient wraps our service connection to Google Play Services and provides access
     * to the user's sign in state as well as the Google APIs.
     */
    protected GoogleApiClient googleApiClient;
    /**
     * Place suggestion adapter.
     */
    private PlaceAutocompleteAdapter placeAutocompleteAdapter;
    /**
     * Event place coordinates
     */
    private LatLng placeLatLng;
    /**
     * Time set
     */
    private LocalTime time;
    /**
     * Date set
     */
    private LocalDate date;

    /**
     * Event label
     */
    private Label label;

    /**
     * List of all labels present in system
     */
    private List<Label> presentLabels;

    /**
     * Configuration handler
     */
    private Configuration configuration;

    /**
     * Callback for results from a Places Geo Data API query that shows the first place result in
     * the details view on screen.
     */
    private ResultCallback<PlaceBuffer> placeDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                Log.e(LOG_TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }
            // Get the Place object from the buffer.
            final Place place = places.get(0);

            Log.i(LOG_TAG, "Place details received: " + place.getName());
            Log.i(LOG_TAG, "Place coordinates: " + place.getLatLng());

            places.release();
        }
    };

    /**
     * Listener that handles selections from suggestions from the AutoCompleteTextView that
     * displays Place suggestions.
     * Gets the place id of the selected item and issues a request to the Places Geo Data API
     * to retrieve more details about the place.
     *
     * @see com.google.android.gms.location.places.GeoDataApi#getPlaceById(
     *com.google.android.gms.common.api.GoogleApiClient, String...)
     */
    private AdapterView.OnItemClickListener placeAutoCompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a AutocompletePrediction from which we
             read the place ID and title.
              */
            final AutocompletePrediction item = placeAutocompleteAdapter.getItem(position);
            final String placeId = item.getPlaceId();
            final CharSequence primaryText = item.getPrimaryText(null);

            Log.i(LOG_TAG, "Autocomplete item selected: " + primaryText);

            // Issue a request to the Places Geo Data API to retrieve a Place object with additional
            // details about the place.
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(googleApiClient, placeId);
            placeResult.setResultCallback(placeDetailsCallback);

            Toast.makeText(getApplicationContext(), "Clicked: " + primaryText,
                    Toast.LENGTH_SHORT).show();
            Log.i(LOG_TAG, "Called getPlaceById to get Place details for " + placeId);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_creation);

        this.configuration = new Configuration(getApplicationContext());
        addPlacePickerUtility();
        addTimePickerUtility();
        addDatePickerUtility();
        addCreateButtonFunction();
        addTagPickerUtility();
        addNewLabelButtonUtility();
    }

    /**
     * Add label utility
     */
    private void addNewLabelButtonUtility() {

        Button addLabelButton = (Button) findViewById(R.id.create_event_new_tag);
        addLabelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final LabelCreationDialog labelCreationDialog = new LabelCreationDialog();
                labelCreationDialog.show(getSupportFragmentManager(), "Create Label");
            }
        });
    }

    /**
     * Adds tag picker utility to activity
     */
    private void addTagPickerUtility() {

        String url = configuration.getScheme() + "://" + configuration.getAuthority() + "/label/read/all";
        Response.Listener<List<Label>> listener = new Response.Listener<List<Label>>() {
            @Override
            public void onResponse(List<Label> response) {

                EventCreation.this.presentLabels = response;
                final AutoCompleteTextView tagSuggestion = (AutoCompleteTextView) findViewById(R.id.create_event_tag);
                final ArrayAdapter<Label> adapter = new ArrayAdapter<>(EventCreation.this,
                        android.R.layout.simple_list_item_1, response);
                tagSuggestion.setAdapter(adapter);

                tagSuggestion.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        label = adapter.getItem(position);
                        Log.i(LOG_TAG, adapter.getItem(position).toString());
                    }
                });
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(LOG_TAG, error.getMessage());
            }
        };

        GsonRequest<List<Label>> request = new GsonRequest<>(url, null,
                new TypeToken<List<Label>>() {
                }.getType(), listener, errorListener);
        PalupSingleton.getInstance(this).getRequestQueue().add(request);
    }

    /**
     * Adds time picker utility to activity
     */
    private void addTimePickerUtility() {

        final ImageButton timePickerButton = (ImageButton) findViewById(R.id.create_event_time_picker);
        timePickerButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                TimePickerFragment timePickerFragment = new TimePickerFragment();
                timePickerFragment.show(getSupportFragmentManager(), "timePicker");
            }
        });
    }

    /**
     * Adds date picker utility to activity
     */
    private void addDatePickerUtility() {
        ImageButton datePickerButton = (ImageButton) findViewById(R.id.create_event_date_picker);
        datePickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment datePickerFragment = new DatePickerFragment();
                datePickerFragment.show(getSupportFragmentManager(), "datePicker");
            }
        });
    }

    /**
     * Place from autocomplete or picker
     */
    private void addPlacePickerUtility() {
        // Construct a GoogleApiClient for the {@link Places#GEO_DATA_API} using AutoManage
        // functionality, which automatically sets up the API client to handle Activity lifecycle
        // events. If your activity does not extend FragmentActivity, make sure to call connect()
        // and disconnect() explicitly.
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0 /* clientId */, this)
                .addApi(Places.GEO_DATA_API)
                .build();

        AutoCompleteTextView placeSuggestionView;
        placeSuggestionView = (AutoCompleteTextView) findViewById(R.id.create_event_place);
        placeSuggestionView.setOnItemClickListener(placeAutoCompleteClickListener);

        // Set up the adapter that will retrieve suggestions from the Places Geo Data API that cover
        // the entire world.
        placeAutocompleteAdapter = new PlaceAutocompleteAdapter(this, googleApiClient, BOUNDS_INDIA, null);
        placeSuggestionView.setAdapter(placeAutocompleteAdapter);

        // Place picker in map to choose place.
        final ImageButton placePicker = (ImageButton) findViewById(R.id.create_event_place_picker);
        placePicker.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(EventCreation.this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    Log.e(LOG_TAG, "Google play services not installed, up-to-date, or enabled");
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    Log.e(LOG_TAG, "Google play service is not available");
                    e.printStackTrace();
                }
                return false;
            }
        });
    }

    /**
     * Create button functionality.
     */
    private void addCreateButtonFunction() {

        final Button createEventButton = (Button) findViewById(R.id.create_event_btn);
        createEventButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validateEventDetails()) {
                    return;
                }



                TextView titleView = (TextView) findViewById(R.id.create_event_title);
                String title = titleView.getText().toString();

                TextView descriptionView = (TextView) findViewById(R.id.create_event_description);
                String description = descriptionView.getText().toString();

                Calendar calendarDate = Calendar.getInstance();
                calendarDate.set(date.getYear(), date.getMonthOfYear(), date.getDayOfMonth(), time.getHourOfDay(), time.getMinuteOfHour());

                final EventCreationDetails event = new EventCreationDetails(title, description,
                        new Location(placeLatLng.latitude, placeLatLng.longitude),
                        calendarDate.getTimeInMillis(), label.getId());

                Response.Listener<String> listener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(LOG_TAG, response);
                        Toast.makeText(EventCreation.this, "Event created successfully", Toast.LENGTH_SHORT).show();
                        Intent intentHome = new Intent(getApplicationContext(), PalupHome.class);
                        startActivity(intentHome);
                    }
                };

                Response.ErrorListener errorListener = new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(LOG_TAG, error.getMessage());
                    }
                };

                Gson gson = new Gson();
                String url = configuration.getScheme() + "://" + configuration.getAuthority() + "/event/create";
                SubmitRequest request = new SubmitRequest(url, gson.toJson(event), listener, errorListener);
                PalupSingleton.getInstance(getApplicationContext()).getRequestQueue().add(request);
            }
        });
    }

    /**
     * @return true if all event details are valid
     */
    private boolean validateEventDetails() {

        TextView titleView = (TextView) findViewById(R.id.create_event_title);
        String title = titleView.getText().toString();
        TextView descriptionView = (TextView) findViewById(R.id.create_event_description);
        String description = descriptionView.getText().toString();
        if (title.equals("")) {
            Toast.makeText(this, "Title can't be empty", Toast.LENGTH_LONG).show();
            return false;
        } else if (description.equals("")) {
            Toast.makeText(this, "description can not be null", Toast.LENGTH_LONG).show();
            return false;
        } else if (placeLatLng == null) {
            Toast.makeText(this, "you must choose a place", Toast.LENGTH_LONG).show();
            return false;
        } else if (time == null) {
            Toast.makeText(this, "you must set event time", Toast.LENGTH_LONG).show();
            return false;
        } else if (date == null) {
            Toast.makeText(this, "you must set event date", Toast.LENGTH_LONG).show();
            return false;
        } else if (label == null) {
            // FIXME: handle the case when user backspace label
            // FIXME: In such a case label shown in textview won't be consistent with label to send
            Toast.makeText(this, "Please set label before you proceed", Toast.LENGTH_SHORT).show();
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                TextView placeBox = (TextView) findViewById(R.id.create_event_place);
                if (place.getPlaceTypes().contains(Place.TYPE_SYNTHETIC_GEOCODE)) {
                    try {
                        Address address = MapUtils.nearestPlace(this, place.getLatLng());
                        placeBox.setText(MapUtils.fullAddress(address));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    placeBox.setText(place.getName());
                }
                placeLatLng = place.getLatLng();
            }
        } else {
            throw new RuntimeException("Unidentified request code");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event_creation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    /**
     * Called when the Activity could not connect to Google Play services and the auto manager
     * could resolve the error automatically.
     * In this case the API is not available and notify the user.
     *
     * @param connectionResult can be inspected to determine the cause of the failure
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(LOG_TAG, "onConnectionFailed: ConnectionResult.getErrorCode() = "
                + connectionResult.getErrorCode());

        // TODO(Developer): Check error code and notify the user of error state and resolution.
        Toast.makeText(this,
                "Could not connect to Google API Client: Error "
                        + connectionResult.getErrorCode(),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onComplete(LocalTime time) {
        this.time = time;
        TextView timeView = (TextView) findViewById(R.id.create_event_time);
        timeView.setText(time.toString("hh:mm a"));
    }

    /**
     * Invoke when event completes
     *
     * @param date set in picker
     */
    @Override
    public void onComplete(LocalDate date) {
        this.date = date;
        TextView dateView = (TextView) findViewById(R.id.create_event_date);
        dateView.setText(date.toString("dd MMM yyyy"));
    }

    /**
     * @return labels present in system
     */
    public List<Label> getPresentLabels() {
        return presentLabels;
    }

    @Override
    public void onComplete(Label label) {
        this.label = label;
        TextView labelView = (TextView) findViewById(R.id.create_event_tag);
        labelView.setText(label.toString());

        AutoCompleteTextView tagSuggestion = (AutoCompleteTextView) findViewById(R.id.create_event_tag);
        ArrayAdapter<Label> adapter = (ArrayAdapter) tagSuggestion.getAdapter();
        adapter.add(label);
        presentLabels.add(label);
    }
}
