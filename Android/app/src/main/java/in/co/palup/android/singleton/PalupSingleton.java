package in.co.palup.android.singleton;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Singleton instance provider in palup
 * <p/>
 * Created by kuldeep on 24/01/16.
 */
public class PalupSingleton {

    /**
     * {@link PalupSingleton} instance
     */
    private static PalupSingleton instance;
    /**
     * Application context
     */
    private static Context context;
    /**
     * Valley http request queue
     */
    private RequestQueue requestQueue;

    private PalupSingleton(Context context) {

        PalupSingleton.context = context;
        this.requestQueue = getRequestQueue();
    }

    /**
     * @param context the application context
     * @return instance of PalupSingleton
     */
    public static synchronized PalupSingleton getInstance(Context context) {

        if (PalupSingleton.instance == null) {
            PalupSingleton.instance = new PalupSingleton(context);
        }
        return instance;
    }

    /**
     * @return volley request queue
     * @see RequestQueue
     */
    public RequestQueue getRequestQueue() {

        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context);
        }

        return requestQueue;
    }
}
