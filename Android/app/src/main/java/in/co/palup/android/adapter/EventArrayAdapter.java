package in.co.palup.android.adapter;

import android.content.Context;
import android.location.Address;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.palup.entity.EventBrief;

import org.joda.time.LocalTime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import in.co.palup.android.R;
import in.co.palup.android.location.MapUtils;

/**
 * Adapter to handle List of EventBriefEventBriefEventBrief
 */
public class EventArrayAdapter extends ArrayAdapter<EventBrief> {

    /**
     * Application Context
     */
    private Context applicationContext;

    /**
     * Activity Context
     */
    private Context activityContext;

    /**
     * Events to be consumed by {@link #getView(int, View, ViewGroup)}
     */
    private List<EventBriefSimple> events;

    public EventArrayAdapter(Context applicationContext, Context activityContext,
                             int resource, List<EventBrief> objects) {
        super(applicationContext, resource, objects);
        this.applicationContext = applicationContext;
        this.activityContext = activityContext;
        this.events = new ArrayList<>();
        for (EventBrief event : objects) {
            events.add(new EventBriefSimple(event));
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        EventBriefViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new EventBriefViewHolder();
            LayoutInflater inflater = (LayoutInflater) applicationContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.event_list_item, parent, false);
            viewHolder.title = (TextView) convertView.findViewById(R.id.event_list_item_title);
            viewHolder.distance = (TextView) convertView.findViewById(R.id.event_list_item_place);
            viewHolder.time = (TextView) convertView.findViewById(R.id.event_list_item_time);
            viewHolder.icon = (ImageView) convertView.findViewById(R.id.event_list_item_icon);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (EventBriefViewHolder) convertView.getTag();
        }

        EventBriefSimple event = events.get(position);

        viewHolder.title.setText(event.getTitle());
        viewHolder.time.setText(event.getTime());
        // FIXME: translate place to distance if location is on
        viewHolder.distance.setText(event.getPlace());
        return convertView;
    }

    /**
     * View holder for EventBrief.
     *
     * @see EventBrief
     */
    private class EventBriefViewHolder {

        TextView title;

        TextView time;

        TextView distance;

        ImageView icon;
    }

    /**
     * Simplified version of {@link EventBrief}.
     * <p/>
     * It is translation of {@link EventBrief}. Because list is processing
     * address, date and time every time when new items come into view,
     * so the performance deteriorates if {@link EventBrief} is used in
     * {@link EventArrayAdapter#getView(int, View, ViewGroup)}.
     * So to improve performance, all fields are translated to Strings which will
     * be directly used by {@link #getView(int, View, ViewGroup)}.
     *
     * @see EventBrief
     */
    private class EventBriefSimple {

        /**
         * {@link EventBrief} id
         */
        private long id;

        /**
         * Title in {@link EventBrief}
         */
        private String title;

        /**
         * Time in {@link EventBrief}
         */
        private String time;

        /**
         * Place in {@link EventBrief}
         */
        private String place;

        public EventBriefSimple(EventBrief eventBrief) {

            this.id = eventBrief.getId();
            this.title = eventBrief.getTitle();
            LocalTime localTime = new LocalTime(eventBrief.getTime());
            this.time = localTime.toString("hh:mm a");
            try {
                Address address = MapUtils.nearestPlace(activityContext, new LatLng(eventBrief.getLocation().getLatitude(), eventBrief.getLocation().getLongitude()));
                this.place = MapUtils.shortAddress(address);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public long getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }

        public String getTime() {
            return time;
        }

        public String getPlace() {
            return place;
        }
    }
}
