package in.co.palup.android.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.widget.TimePicker;

import org.joda.time.LocalTime;

import java.util.Calendar;

/**
 * Time picker
 * <p>
 * Created by Kuldeep Yadav on 25-Nov-15.
 */
public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    /**
     * Listener to respond on complete event
     */
    private OnCompleteListener onCompleteListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.onCompleteListener = (OnCompleteListener) activity;
    }

    /**
     * Called when the user is done setting a new time and the dialog has
     * closed.
     *
     * @param view      the view associated with this listener
     * @param hourOfDay the hour that was set
     * @param minute    the minute that was set
     */
    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        onCompleteListener.onComplete(new LocalTime(hourOfDay, minute));
    }

    /**
     * Listener for on complete event
     */
    public interface OnCompleteListener {

        /**
         * Invoke when event completes
         *
         * @param time set in picker
         */
        void onComplete(LocalTime time);
    }
}
