package in.co.palup.android.dialog;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.palup.entity.Label;
import com.palup.entity.LabelCreationDetails;

import java.util.List;

import in.co.palup.android.EventCreation;
import in.co.palup.android.R;
import in.co.palup.android.singleton.PalupSingleton;
import in.co.palup.android.utilities.Configuration;
import in.co.palup.android.volley.GsonRequest;

/**
 * Label creation dialog
 * <p/>
 * Created by Kuldeep Yadav on 13/12/15.
 */
public class LabelCreationDialog extends DialogFragment {

    /**
     * Logging tag
     */
    private final String TAG = "Label Creation";

    /**
     * Listener on completion of event
     */
    private OnCompleteListener onCompleteListener;

    /**
     * Configuration handle
     */
    private Configuration configuration;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        this.configuration = new Configuration(getContext());
        this.onCompleteListener = (EventCreation) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.dialog_label_creation, container, false);
        getDialog().setTitle("Label Creation");

        addParentLabelUtility(rootView);
        addSubmitButtonUtility(rootView);

        return rootView;
    }

    /**
     * @param rootView for fragment layout
     */
    private void addParentLabelUtility(View rootView) {
        List<Label> labels = ((EventCreation) getActivity()).getPresentLabels();
        ArrayAdapter<Label> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, labels);
        Spinner spinner = (Spinner) rootView.findViewById(R.id.label_creation_parent);
        spinner.setAdapter(adapter);
    }

    /**
     * @param name          of label
     * @param parentLabelId id of parent label
     * @return true if label is not empty and parent label is not null
     */
    private boolean validate(String name, String parentLabelId) {
        return name != null && !name.isEmpty() && parentLabelId != null;
    }

    /**
     * @param rootView for fragment layout
     */
    private void addSubmitButtonUtility(View rootView) {

        final Spinner spinner = (Spinner) rootView.findViewById(R.id.label_creation_parent);
        final TextView nameView = (TextView) rootView.findViewById(R.id.label_creation_name);
        Button submitButton = (Button) rootView.findViewById(R.id.label_creation_submit);
        // TODO : validate before submitting data
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, spinner.getSelectedItem().toString());
                Log.i(TAG, nameView.getText().toString());

                final String name = nameView.getText().toString();
                final String parentLabelId = ((Label) spinner.getSelectedItem()).getId();
                if (!validate(name, parentLabelId)) {
                    Toast.makeText(getContext(), "Set label", Toast.LENGTH_SHORT).show();
                    return;
                }

                String url = configuration.getScheme() + "://" + configuration.getAuthority() + "/label/create";

                Response.Listener listener = new Response.Listener<Label>() {
                    @Override
                    public void onResponse(Label response) {
                        Log.i(TAG, response.toString());
                        onCompleteListener.onComplete(response);
                        LabelCreationDialog.this.dismiss();
                    }
                };

                Response.ErrorListener errorListener = new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), "Label creation failed", Toast.LENGTH_SHORT).show();
                        Log.e(TAG, error.getMessage());
                    }
                };

                LabelCreationDetails labelCreationDetails =
                        new LabelCreationDetails(name, parentLabelId);

                final Gson gson = new Gson();
                GsonRequest request = new GsonRequest(url, gson.toJson(labelCreationDetails),
                        new TypeToken<Label>() {
                        }.getType(), listener, errorListener);
                PalupSingleton.getInstance(getContext()).getRequestQueue().add(request);
            }
        });
    }

    /**
     * Listener for on complete event.
     */
    public interface OnCompleteListener {

        /**
         * Invoke when event completes
         *
         * @param label received from fragment
         */
        void onComplete(Label label);
    }
}
