package in.co.palup.android;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.reflect.TypeToken;
import com.palup.entity.Event;
import com.palup.entity.Location;

import org.joda.time.LocalTime;

import java.io.IOException;

import in.co.palup.android.location.MapUtils;
import in.co.palup.android.singleton.PalupSingleton;
import in.co.palup.android.utilities.Configuration;
import in.co.palup.android.volley.GsonRequest;

/**
 * Activity to show details of event.
 *
 * @author kuldeepwrites@gmail.com
 */
public class EventDetails extends Activity {

    /**
     * Logger tag for the class
     */
    private static final String LOG_TAG = "Event Details";

    /**
     * Configuration handle
     */
    private Configuration configuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);

        this.configuration = new Configuration(getApplicationContext());
        long id = getIntent().getLongExtra("id", -1);
        fetchEvent(id);
    }

    /**
     * Set click listener on Map Pin button.
     */
    private void registerActionOnMapPinClick(final Location location) {

        ImageButton placePin = (ImageButton) findViewById(R.id.event_place_pin);
        placePin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri = "geo:" + location.getLatitude() + "," + location.getLongitude() +
                        "?q=" + location.getLatitude() + "," + location.getLongitude();
                Uri googleMapIntentUri = Uri.parse(uri);
                Intent googleMapIntent = new Intent(Intent.ACTION_VIEW, googleMapIntentUri);
                googleMapIntent.setPackage("com.google.android.apps.maps");
                if (googleMapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(googleMapIntent);
                }
            }
        });
    }

    /**
     * Fetch event from server
     *
     * @param id of event to be fetched
     */
    private void fetchEvent(final long id) {

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(LOG_TAG, error.getMessage());
            }
        };

        Response.Listener<Event> listener = new Response.Listener<Event>() {
            @Override
            public void onResponse(Event response) {
                Log.i(LOG_TAG, response.getDescription());

                TextView title = (TextView) findViewById(R.id.event_title);
                title.setText(response.getTitle());
                TextView time = (TextView) findViewById(R.id.event_time);
                LocalTime localTime = new LocalTime(response.getTime());
                time.setText(localTime.toString("hh:mm a"));
                TextView place = (TextView) findViewById(R.id.event_place);
                try {
                    Address address = MapUtils.nearestPlace(EventDetails.this,
                            new LatLng(response.getLocation().getLatitude(),
                                    response.getLocation().getLongitude()));
                    Log.i(LOG_TAG, MapUtils.shortAddress(address));
                    place.setText(MapUtils.fullAddress(address));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                TextView description = (TextView) findViewById(R.id.event_description);
                description.setText(response.getDescription());
                registerActionOnMapPinClick(response.getLocation());
            }
        };

        String url = configuration.getScheme() + "://" + configuration.getAuthority() + ("/event/read/event/" + id);
        GsonRequest<Event> request = new GsonRequest<>(url, null,
                new TypeToken<Event>() {
                }.getType(), listener, errorListener);

        PalupSingleton.getInstance(this).getRequestQueue().add(request);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
