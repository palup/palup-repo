package in.co.palup.android.location;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Utilities related to maps, locations and places
 * <p>
 * Created by Kuldeep Yadav on 29-Nov-15.
 */
public class MapUtils {

    /**
     * @param location coordinates on map
     * @return nearest address for the location
     */
    public static Address nearestPlace(Context context, LatLng location) throws IOException {

        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses = geocoder.getFromLocation(location.latitude, location.longitude, 1);
        if (addresses.size() != 1) {
            throw new RuntimeException("Unique address is not found");
        }
        return addresses.get(0);
    }

    /**
     * @param googleAddress of the place
     * @return address in short
     */
    public static String shortAddress(Address googleAddress) {
        return googleAddress.getAddressLine(0) + ", " + googleAddress.getAddressLine(1);
    }

    /**
     * @param googleAddress of the place
     * @return full address
     */
    public static String fullAddress(Address googleAddress) {
        StringBuilder builder = new StringBuilder();
        int numberOfAddressLines = googleAddress.getMaxAddressLineIndex();
        for (int i = 0; i <= numberOfAddressLines; i++) {
            builder.append(googleAddress.getAddressLine(i));
            if (i != numberOfAddressLines)
                builder.append(", ");
        }
        return builder.toString();
    }
}
